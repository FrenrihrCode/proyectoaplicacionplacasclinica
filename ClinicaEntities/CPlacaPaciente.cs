﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicaEntities
{
    public class CPlacaPaciente
    {
        public int Codigo { get; set; }
        public int Orden { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public int Celular { get; set; }
        public string Medico { get; set; }
        public string Aseguradora { get; set; }
        public string Procedencia { get; set; }
        public string Examen { get; set; }
        public string Estado { get; set; }
        public Nullable<System.DateTime> FechaSolicitud { get; set; }
        public int Edad { get; set; }
        public string Imagen { get; set; }
        public string Informe { get; set; }
    }
}
