﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClinicaEntities;
using ClinicaLTS;

namespace ClinicaFormDesktop
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string estado = cbxEstado.Text.ToUpper();
            string celular = txtCelular.Text;
            string orden = txtOrden.Text;
            string imagenPath = txtImagen.Text;
            string informePath = txtInforme.Text;
            string email = txtEmail.Text;
            
            if (estado.Length != 0 && imagenPath != "" && informePath != "" && email != "")
            {
                if (int.TryParse(orden, result: out _) && int.TryParse(celular, result: out _))
                {
                    CPlacaPacienteLTS lts = new CPlacaPacienteLTS();
                    CPlacaPaciente placaPaciente = new CPlacaPaciente
                    {
                        Nombre = txtNombre.Text,
                        Estado = estado == "SOLICITADO" ? "S" : "R",
                        Examen = txtExamen.Text,
                        FechaSolicitud = dtpFecha.Value,
                        Imagen = imagenPath,
                        Informe = informePath,
                        Email = email,
                        Celular = int.Parse(celular),
                        Orden = int.Parse(orden),
                        Medico = txtMedico.Text,
                        Procedencia = txtProcedencia.Text,
                        Aseguradora = txtAseguradora.Text,
                        Edad = int.Parse(numEdad.Value.ToString()),
                    };
                    bool res = lts.Insertar(placaPaciente);
                    if (res)
                    {
                        LoadData();
                        MessageBox.Show("Nuevo registro añadido");
                    } else
                    {
                        MessageBox.Show("Ha ocurrido un error");
                    }
                }
                else
                {
                    lblError.Text = "Datos incorrectos";
                }
            } else
            {
                lblError.Text = "Faltan datos";
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text;
            string estado = cbxEstado.Text.ToUpper();
            string celular = txtCelular.Text;
            string orden = txtOrden.Text;
            string imagenPath = txtImagen.Text;
            string informePath = txtInforme.Text;
            string email = txtEmail.Text;

            if (estado.Length != 0 && imagenPath != "" && informePath != ""
                && email != "" && codigo != "")
            {
                if (int.TryParse(orden, result: out _) && int.TryParse(celular, result: out _)
                     && int.TryParse(codigo, result: out _))
                {
                    CPlacaPacienteLTS lts = new CPlacaPacienteLTS();
                    CPlacaPaciente placaPaciente = new CPlacaPaciente
                    {
                        Codigo = int.Parse(codigo),
                        Nombre = txtNombre.Text,
                        Estado = estado == "SOLICITADO" ? "S" : "R",
                        Examen = txtExamen.Text,
                        FechaSolicitud = dtpFecha.Value,
                        Imagen = imagenPath,
                        Informe = informePath,
                        Email = email,
                        Celular = int.Parse(celular),
                        Orden = int.Parse(orden),
                        Medico = txtMedico.Text,
                        Procedencia = txtProcedencia.Text,
                        Aseguradora = txtAseguradora.Text,
                        Edad = int.Parse(numEdad.Value.ToString()),
                    };
                    bool res = lts.Modificar(placaPaciente);
                    if (res)
                    {
                        LoadData();
                        MessageBox.Show($"Registro {codigo} modificado");
                    }
                    else
                    {
                        MessageBox.Show("Ha ocurrido un error");
                    }
                }
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text;
            if (int.TryParse(codigo, result: out _))
            {
                DialogResult dialogResult = 
                    MessageBox.Show($"¿Está Ud. seguro que desea eliminar el registro {codigo}?",
                    "Eliminar?", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    CPlacaPacienteLTS lts = new CPlacaPacienteLTS();
                    bool res = lts.Eliminar(int.Parse(codigo));
                    if (res)
                    {
                        LoadData();
                        MessageBox.Show($"Registro {codigo} eliminado");
                    }
                    else
                    {
                        MessageBox.Show("Ha ocurrido un error");
                    }
                }
            }
            else
            {
                lblError.Text = "Faltan especificar código";
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text;
            CPlacaPacienteLTS lts = new CPlacaPacienteLTS();
            if (int.TryParse(codigo, result: out _))
            {
                dgvDatosPlacas.DataSource = lts.Buscar(int.Parse(codigo));
            } else
            {
                dgvDatosPlacas.DataSource = lts.ListarDesktop();
            }
            dgvDatosPlacas.Refresh();
            dgvDatosPlacas.Rows[0].Selected = true;
        }

        private void dgvDatosPlacas_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDatosPlacas.SelectedRows.Count > 0)
            {
                string estado = dgvDatosPlacas.SelectedRows[0].Cells[9].Value.ToString();
                txtCodigo.Text = dgvDatosPlacas.SelectedRows[0].Cells[0].Value.ToString();
                txtOrden.Text = dgvDatosPlacas.SelectedRows[0].Cells[1].Value.ToString();
                txtNombre.Text = dgvDatosPlacas.SelectedRows[0].Cells[2].Value.ToString();
                txtEmail.Text = dgvDatosPlacas.SelectedRows[0].Cells[3].Value.ToString();
                txtCelular.Text = dgvDatosPlacas.SelectedRows[0].Cells[4].Value.ToString();
                txtMedico.Text = dgvDatosPlacas.SelectedRows[0].Cells[5].Value.ToString();
                txtAseguradora.Text = dgvDatosPlacas.SelectedRows[0].Cells[6].Value.ToString();
                txtProcedencia.Text = dgvDatosPlacas.SelectedRows[0].Cells[7].Value.ToString();
                txtExamen.Text = dgvDatosPlacas.SelectedRows[0].Cells[8].Value.ToString();
                cbxEstado.Text = estado == "S" ? "Solicitado" : "Realizado";
                dtpFecha.Text = dgvDatosPlacas.SelectedRows[0].Cells[10].Value.ToString();
                numEdad.Text = dgvDatosPlacas.SelectedRows[0].Cells[11].Value.ToString();
                txtImagen.Text = dgvDatosPlacas.SelectedRows[0].Cells[12].Value.ToString();
                txtInforme.Text = dgvDatosPlacas.SelectedRows[0].Cells[13].Value.ToString();
                picPlacaImg.Image = Image.FromFile(dgvDatosPlacas.SelectedRows[0].Cells[12].Value.ToString());
            }
        }

        private void btnInforme_Click(object sender, EventArgs e)
        {
            ofdPlacaFile.InitialDirectory = "c:\\";
            ofdPlacaFile.FileName = "Informe.pdf";
            ofdPlacaFile.Filter = "Pdf files (*.pdf)|*.pdf";
            if (ofdPlacaFile.ShowDialog() == DialogResult.OK)
            {
                txtInforme.Text = ofdPlacaFile.FileName;
            }
        }

        private void btnImagen_Click(object sender, EventArgs e)
        {
            ofdPlacaFile.InitialDirectory = "c:\\";
            ofdPlacaFile.FileName = "placa.png";
            ofdPlacaFile.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.svg";
            try
            {
                if (ofdPlacaFile.ShowDialog() == DialogResult.OK)
                {
                    txtImagen.Text = ofdPlacaFile.FileName;
                    picPlacaImg.Image = Image.FromFile(ofdPlacaFile.FileName);
                }
            }
            catch
            {
                MessageBox.Show("El archivo seleccionado no es un tipo de imagen válido");
            }
        }
        private void LoadData()
        {
            CPlacaPacienteLTS lts = new CPlacaPacienteLTS();
            dgvDatosPlacas.DataSource = lts.ListarDesktop();
            dgvDatosPlacas.Refresh();
            lblError.Text = "";
        }
    }
}
