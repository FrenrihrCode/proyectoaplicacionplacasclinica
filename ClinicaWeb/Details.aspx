﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Details.aspx.cs" Inherits="ClinicaWeb.Details"  %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <section class="jumbotron text-center">
            <asp:formview id="detailDataView" runat="server">
        <itemtemplate>
        <div class="container row">
          <div class="col text-center">
          <h1 class="jumbotron-heading"><%# Eval("Codigo") %> - <%# Eval("Nombre") %></h1>
          <p class="lead text-muted"><%# Eval("Estado") %></p>
          <p class="lead text-muted"><%# Eval("Examen") %></p>
          <p class="lead text-muted"><%# Eval("FechaSolicitud") %></p>
          <p>
            <a href="mailto:<%# Eval("Email") %>?Subject=Placas%clínica" class="btn btn-primary my-2">Enviar Correo</a>
            <a href="https://api.whatsapp.com/send?phone=<%# Eval("Celular") %>&text=Hola%le%hablamos%de%la%clínica."
                target="_blank" class="btn btn-secondary my-2">Enviar Whatsapp</a>
          </p>
              </div>
          <div class="col">
              <asp:Image class="img-thumbnail" ID="imagePlaca"
                  runat="server" ImageUrl='<%# previewImg(Eval("Imagen").ToString()) %>' />
            <a href="<%# Eval("Informe") %>" target="_blank" class="btn btn-secondary my-2">Ver Informe</a>
          </p>
            </div>
        </div>
            </itemtemplate>
            </asp:formview>
    </section>
</asp:Content>
