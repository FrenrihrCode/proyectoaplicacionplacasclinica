﻿<%@ Page Title="Lista de Placas" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ClinicaWeb._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <section>
        <div class="container">
            <hgroup>
                <h2><%: Page.Title %></h2>
            </hgroup>
            <asp:ListView ID="productList" runat="server" GroupItemCount="3">
                <EmptyDataTemplate>
                    <h4>No se encontró ningún registro!</h4>
                </EmptyDataTemplate>
                <GroupTemplate>
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </GroupTemplate>
                <ItemTemplate>
                    <td runat="server">
                            <div class="card text-center m-1" style="width: 18rem;">
                        <div class="card-header"><%# Eval("Codigo") %></div>
                      <div class="card-body">
                        <h5 class="card-title"><%# Eval("Nombre") %></h5>
                        <p class="card-text">Tipos de paciente: <%# Eval("Estado") %></p>
                        <p class="card-text">Fecha:<%# Eval("FechaSolicitud") %></p>
                       <a href="Details.aspx?id=<%# Eval("Codigo") %>" class="btn btn-primary">Más detalles...</a>
                      </div>
                    </div>
                    </td>
                    
                </ItemTemplate>
                <LayoutTemplate>
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="groupPlaceholderContainer" runat="server" style="width:100%">
                                        <tr id="groupPlaceholder"></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr></tr>
                        </tbody>
                    </table>
                </LayoutTemplate>
            </asp:ListView>
        </div>
    </section>
</asp:Content>
