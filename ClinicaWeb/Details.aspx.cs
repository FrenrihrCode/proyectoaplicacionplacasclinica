﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClinicaBLL;
using ClinicaEntities;
using System.IO;

namespace ClinicaWeb
{
    public partial class Details : System.Web.UI.Page
    {
        CPlacaPacienteBLL cPlaca = new CPlacaPacienteBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            if (!String.IsNullOrWhiteSpace(id))
            {
                List<CPlacaPaciente> placas = new List<CPlacaPaciente>();
                placas.Add(cPlaca.GetPlacaBLL(id));
                detailDataView.DataSource = placas;
                detailDataView.DataBind();
            } else {
                Response.Redirect("/");
            }
        }

        protected string previewImg(string filePath)
        {
            string imgUrl = "";
            if (File.Exists(filePath))
            {
                Console.WriteLine(filePath);
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();
                string base64 = Convert.ToBase64String(bytes, 0, bytes.Length);
                imgUrl = "data:image/png;base64," + base64;
            }
            return imgUrl;
        }
        protected string previewPdf(string filePath)
        {
            string imgUrl = "";
            if (File.Exists(filePath))
            {
                Console.WriteLine(filePath);
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();
                string base64 = Convert.ToBase64String(bytes, 0, bytes.Length);
                imgUrl = "data:application/pdf;base64," + base64;
            }
            return imgUrl;
        }
    }
}