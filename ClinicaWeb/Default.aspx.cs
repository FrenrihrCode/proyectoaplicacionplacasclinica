﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClinicaBLL;
using ClinicaEntities;

namespace ClinicaWeb
{
    public partial class _Default : Page
    {
        CPlacaPacienteBLL cPlaca = new CPlacaPacienteBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            productList.DataSource = cPlaca.ListarBLL();
            productList.DataBind();
        }
    }
}