﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicaEntities;

namespace ClinicaLTS
{
    public class CPlacaPacienteLTS
    {
        readonly ClinicaDataContext context = new ClinicaDataContext();
        public List<CPlacaPaciente> ListarDesktop()
        {
            try
            {
                List<CPlacaPaciente> listPlacas = new List<CPlacaPaciente>();
                var query = from p in context.PacientePlaca
                            select p;
                foreach (var p in query)
                {
                    CPlacaPaciente placa = new CPlacaPaciente
                    {
                        Codigo = p.Codigo,
                        Nombre = p.Nombre,
                        Estado = p.Estado,
                        Examen = p.Examen,
                        FechaSolicitud = p.FechaSolicitud,
                        Imagen = p.Imagen,
                        Informe = p.Informe,
                        Email = p.Email,
                        Celular = p.Celular,
                        Orden = p.Orden,
                        Medico = p.Medico,
                        Procedencia = p.Procedencia,
                        Aseguradora = p.Aseguradora,
                        Edad = Byte.Parse(p.Edad.ToString())
                    };
                    listPlacas.Add(placa);
                }
                return listPlacas;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool Insertar(CPlacaPaciente p)
        {
            try
            {
                PacientePlaca placa = new PacientePlaca
                {
                    Nombre = p.Nombre,
                    Estado = p.Estado,
                    Examen = p.Examen,
                    FechaSolicitud = p.FechaSolicitud,
                    Imagen = p.Imagen,
                    Informe = p.Informe,
                    Email = p.Email,
                    Celular = p.Celular,
                    Orden = p.Orden,
                    Medico = p.Medico,
                    Procedencia = p.Procedencia,
                    Aseguradora = p.Aseguradora,
                    Edad = Byte.Parse(p.Edad.ToString())
                };
                context.PacientePlaca.InsertOnSubmit(placa);
                context.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool Modificar(CPlacaPaciente p)
        {
            try
            {
                PacientePlaca placa = (from cp in context.PacientePlaca
                             where cp.Codigo == p.Codigo
                             select cp).FirstOrDefault();
                placa.Nombre = p.Nombre;
                placa.Estado = p.Estado;
                placa.Examen = p.Examen;
                placa.FechaSolicitud = p.FechaSolicitud;
                placa.Imagen = p.Imagen;
                placa.Informe = p.Informe;
                placa.Email = p.Email;
                placa.Celular = p.Celular;
                placa.Orden = p.Orden;
                placa.Medico = p.Medico;
                placa.Procedencia = p.Procedencia;
                placa.Aseguradora = p.Aseguradora;
                placa.Edad = Byte.Parse(p.Edad.ToString());
                context.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool Eliminar(int codigo)
        {
            try
            {
                PacientePlaca placa = (from cp in context.PacientePlaca
                                       where cp.Codigo == codigo
                                       select cp).FirstOrDefault();
                context.PacientePlaca.DeleteOnSubmit(placa);
                context.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public List<CPlacaPaciente> Buscar(int codigo)
        {
            try
            {
                List<CPlacaPaciente> listPlacas = new List<CPlacaPaciente>();
                var query = from p in context.PacientePlaca
                            where p.Codigo == codigo select p;
                foreach (var p in query)
                {
                    CPlacaPaciente placa = new CPlacaPaciente
                    {
                        Codigo = p.Codigo,
                        Nombre = p.Nombre,
                        Estado = p.Estado,
                        Examen = p.Examen,
                        FechaSolicitud = p.FechaSolicitud,
                        Imagen = p.Imagen,
                        Informe = p.Informe,
                        Email = p.Email,
                        Celular = p.Celular,
                        Orden = p.Orden,
                        Medico = p.Medico,
                        Procedencia = p.Procedencia,
                        Aseguradora = p.Aseguradora,
                        Edad = Byte.Parse(p.Edad.ToString())
                    };
                    listPlacas.Add(placa);
                }
                return listPlacas;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

        }
    }
}
