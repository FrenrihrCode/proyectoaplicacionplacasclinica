﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicaDAL;
using ClinicaEntities;

namespace ClinicaBLL
{
    public class CPlacaPacienteBLL
    {
        CPlacaPacienteDAL placa = new CPlacaPacienteDAL();

        public List<CPlacaPaciente> ListarBLL()
        {
            return placa.ListarPlacasWeb();
        }

        public CPlacaPaciente GetPlacaBLL(string codigo)
        {
            if (int.TryParse(codigo, out _))
            {
                return placa.GetData(int.Parse(codigo));
            }
            return null;
        }
    }
}
