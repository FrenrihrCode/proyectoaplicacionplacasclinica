﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicaEntities;

namespace ClinicaDAL
{
    public class CPlacaPacienteDAL
    {
        public List<CPlacaPaciente> ListarPlacasWeb()
        {
            using (ClinicaEntities context = new ClinicaEntities())
            {
                var q = context.PacientePlaca.Select(p => new CPlacaPaciente
                {
                    Codigo = p.Codigo,
                    Nombre = p.Nombre,
                    Estado = p.Estado,
                    Examen = p.Examen,
                    FechaSolicitud = p.FechaSolicitud,
                    Imagen = p.Imagen,
                    Informe = p.Informe,
                    Email = p.Email,
                    Celular = p.Celular
                });
                return q.ToList();
            }
        }

        public CPlacaPaciente GetData(int codigo)
        {
            using (ClinicaEntities context = new ClinicaEntities())
            {
                CPlacaPaciente placa = context.PacientePlaca.Where(c => c.Codigo == codigo)
                    .Select(p => new CPlacaPaciente
                    {
                        Codigo = p.Codigo,
                        Nombre = p.Nombre,
                        Estado = p.Estado == "S" ? "Solicitado" : "Realizado",
                        Examen = p.Examen,
                        FechaSolicitud = p.FechaSolicitud,
                        Imagen = p.Imagen,
                        Informe = p.Informe,
                        Email = p.Email,
                        Celular = p.Celular
                    }).FirstOrDefault();
                return placa;
            }
        }
    }
}
